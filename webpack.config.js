const path = require("path");

module.exports = function (env) {
  let dev = env.dev ? true : false;

  return {
    mode: dev ? "development" : "production",
    entry: "./src/axios.js",
    output: {
      filename: dev ? "axios.js" : "axios.min.js",
      path: path.resolve(__dirname, "dist"),
      sourceMapFilename: dev ? "[file].map[query]" : "[file].min.map[query]",
      library: {
        name: "axios",
        type: "umd",
      },
      globalObject: "this",
    },
    devtool: "source-map",
    devServer: {
      port: 8000,
      open: true,
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-env"],
            },
          },
        },
      ],
    },
  };
};
