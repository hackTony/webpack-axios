我们已经知道了 axios 的核心代码和流程，完全可以自己造一个轮子

它需要的几个核心部分如下：

- Axios 构造函数
- 拦截器
- dispatchRequest
- adapter

如果要一个极简核心的话，我们只需要 request 请求函数，和一个 XMLHttpRequest 即可，拦截器，适配器 dispatchRquest 都可以不要

- Axios 构造函数
