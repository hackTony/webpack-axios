// 入口文件
import Axios from "./core/Axios";
import utils from "./utils";
import mergeConfig from "./core/mergeConfig";
import defaults from "./defaults";

function createInstance(defaultConfig) {
  let context = new Axios(defaultConfig);
  let instance = utils.bind(Axios.prototype.request, context);

  // extend prototype
  utils.extend(instance, Axios.prototype, context);
  // extend Axios属性
  utils.extend(instance, context);

  instance.create = function (newConfig) {
    return createInstance(mergeConfig(defaultConfig, newConfig));
  };

  return instance;
}

let axios = createInstance(defaults);

console.dir(axios);

// console.log(axios);

// axios({
//   url: "http://localhost:3000/posts",
//   method: "get",
// }).then((response) => {
//   console.log(response);
// });

axios
  .get("http://localhost:3000/posts", {
    headers: {
      common: 111,
    },
  })
  .then((response) => {
    console.log(response);
  });
