import utils from "../utils";

// 合并两个配置对象，返回一个新的对象
function mergeConfig(config1, config2) {
  let config = {};

  let axiosKeys = Array.from(
    new Set(Object.keys(config1).concat(Object.keys(config2)))
  );

  // 优先使用config2身上的
  utils.forEach(axiosKeys, function (prop) {
    if (utils.isObject(config2[prop])) {
      config[prop] = utils.merge(config1[prop], config2[prop]);
    } else if (typeof config2[prop] !== "undefined") {
      config[prop] = config2[prop];
    } else if (utils.isObject(config1[prop])) {
      config[prop] = utils.merge(config1[prop]);
    } else if (typeof config1[prop] !== "undefined") {
      config[prop] = config1[prop];
    }
  });

  return config;
}

export default mergeConfig;
